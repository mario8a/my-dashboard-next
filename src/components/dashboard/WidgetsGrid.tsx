"use client"
import React from 'react'
import { SimpleWidget } from '..'
import { IoCartOutline } from 'react-icons/io5';
import { useAppSelector } from '@/store';

export const WidgetsGrid = () => {

  const isCart = useAppSelector(state => state.counter.count);

  return (
    <div className='flex flex-wrap p-2 items-center'>
        <SimpleWidget
          title={`${isCart}`}
          subtitle='Productos agregados'
          label='Contador'
          href='/dashboard/counter'
          icon={<IoCartOutline size={70} className='text-blue-600' />}
        />
        {/* <SimpleWidget /> */}
      </div>
  )
}
