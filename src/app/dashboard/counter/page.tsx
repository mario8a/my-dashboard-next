import { CartCounter } from "@/shopping-cart"

export const metadata = {
  title: 'Shopping page',
  description: 'Simple counter app'
}

const CounterPage = () => {
  

  return (
    <div className="flex flex-col items-center justify-center w-full h-full">
      <span>Productos</span>
      <CartCounter value={5} />
    </div>
  )
}

export default CounterPage