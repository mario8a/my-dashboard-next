import { FavoritePokemons } from "@/pokemons/components/FavoritePokemons";

export const metadata = {
  titlle: "Favoritos",
  description: "Add favorite pokemon"
}


export default async function PokemonsPage() {

 
  return (
    <div className="flex flex-col">
      <span className="text-5xl my-2">Pokemons favoritos<small className="text-blue-500">Global state</small></span>
      
      <FavoritePokemons />
    </div>
  )
}
